namespace :send_email do
    include Rails.application.routes.url_helpers

    desc "Auto send email events"
    task auto: :environment do
        if SendEmail.all.first.try(:auto_date).to_s == Time.zone.today.day.to_s
            settings = SendEmail.first
            if settings.auto_send.to_s == "true"
                type = "auto"
                excluded_emails = settings.auto_excluded_emails
                frecuence = settings.auto_frecuence

                if settings.auto_user_type != 2
                    user_type = "Holder"
                    Holder.all.each do |holder|
                        send_mail(holder, user_type, type, excluded_emails, settings)
                    end
                end
                if settings.auto_user_type != 1
                    user_type = "Manager"
                    Manage.all.each do |m|
                        if !User.find(m.user_id).email.blank?
                            send_mail(m, user_type, type, excluded_emails, settings)
                        end
                    end
                end
            else
                puts "El envío automático de emails está desactivado."
            end
        end   
    end

    desc "Manual send email events"
    task manual: :environment do
        settings = SendEmail.first
        if settings.manual_send.to_s == "true"
            type = "manual"
            excluded_emails = settings.manual_excluded_emails
            send_user_type = settings.manual_user_type

            if settings.manual_user_type != 2
                user_type = "Holder"
                Holder.all.each do |holder|
                    send_mail(holder, user_type, type, excluded_emails, settings)
                end
            end
            if settings.manual_user_type != 1
                user_type = "Manager"
                Manage.all.each do |m|
                    if !User.find(m.user_id).email.blank?
                        send_mail(m, user_type, type, excluded_emails, settings)
                    end
                end
            end
        else
            puts "El envío manual de emails está desactivado."
        end
    end

    def data_mail(mail, holder, ids, events, date_title)
        if Rails.env.staging? 
            default_url_options[:host] = 'tomadedecisionesdesa.munimadrid.es'
        elsif Rails.env.preproduction? 
            default_url_options[:host] = 'tomadedecisionespre.munimadrid.es'
        elsif Rails.env.production?
            default_url_options[:host] = 'tomadedecisiones.munimadrid.es'
        else
            default_url_options[:host] = 'localhost:3000'
        end
        mail.subject = mail.subject.gsub!("@holder@", holder.full_name)
        body = "<style>
                #events {
                    font-family: Arial, Helvetica, sans-serif;
                    border-collapse: collapse;
                    width: 100%;
                }
                
                #events td, #events th {
                    border: 1px solid #ddd;
                    padding: 8px;
                }
                
                #events tr:nth-child(even){background-color: #f2f2f2;}
                
                #events tr:hover {background-color: #ddd;}
                
                #events th {
                    padding-top: 12px;
                    padding-bottom: 12px;
                    text-align: left;
                    background-color: #003df6;
                    color: white;
                }
                </style>
                <table id='events'><tr><th>Fecha</th><th>Título evento</th><th>Actividad de lobby</th></tr>"
        events.each { |e| body = body + "<tr>
            <td>#{e.try(:scheduled).to_date}</td>
            <td><a href='#{show_url(e)}'>#{e.try(:title).to_s}</a></td>
            <td>#{e.try(:lobby_activity).to_s == 'true' ? 'Si' : 'No'}</td></tr>"
        }
        body = body + "</table>"
        mail.email_body = mail.email_body.gsub("@date@", date_title).gsub("@holder@", holder.full_name).gsub("@events@", body)
    end

    def send_mail(resource, user_type, type, excluded_emails, settings)
        puts "Envio de Holder...."
        if user_type.to_s == "Holder" && !resource.user_email.blank? && !excluded_emails.include?(resource.user_email)

            ids = Position.where(holder_id: resource.id).ids
            if !ids.blank?
                events = get_events(ids, type, settings)
                puts events.count
                if !events.blank? 
                    puts "Entraaa"
                    date_title = get_date_title(type, settings)
                    mail = ManageEmail.find_by(type_data: 'Events')
                    if !mail.blank?
                        data_mail(mail,resource,ids,events,date_title)
                        puts "Email enviado al titular: #{resource.id}"
                        begin
                            ['jimenezel@madrid.es', 'registrodelobbies@madrid.es', 'transparenciaydatos@madrid.es', 'crespodhe@madrid.es',
                            'ajmorenoh@minsait.com'].each do |email|
                                    puts ManageMailer.sender(email,mail).deliver_now
                                end
                            # ManageMailer.sender(resource.user_email,mail).deliver_now
                        rescue =>e
                            begin
                                Rails.logger.error("SMS-ERROR: #{e}")
                            rescue
                            end
                        end
                    end
                end
            end
        end
        puts "Envio de Manager...."
        if user_type.to_s == "Manager" && !excluded_emails.include?(User.find(resource.user_id).email)        
            ids = Position.where(holder_id: resource.holder_id).ids
            if !ids.blank?
                events = get_events(ids, type, settings)
                if !events.blank?
                    date_title = get_date_title(type, settings)
                    mail = ManageEmail.find_by(type_data: 'Events')
                    if !mail.blank?
                        data_mail(mail,Holder.find(resource.holder_id), ids,events,date_title)
                        puts "Email enviado al gestor: #{resource.user_id}"
                        begin
                             ['jimenezel@madrid.es', 'registrodelobbies@madrid.es', 'transparenciaydatos@madrid.es', 'crespodhe@madrid.es',
                                'ajmorenoh@minsait.com'].each do |email|
                                    puts ManageMailer.sender(email,mail).deliver_now
                                end
                            #ManageMailer.sender(User.find(resource.user_id).email,mail).deliver_now
                        rescue => e
                            begin
                                Rails.logger.error("SMS-ERROR: #{e}")
                            rescue
                            end
                        end
                    end
                end
            end
        end
    end

    def get_events(ids, type, settings)
        events = []
        if type == "auto"
            case settings.auto_frecuence
            when 1
                date = Time.zone.today - 1.month
            when 2
                date = Time.zone.today - 2.months
            when 3
                date = Time.zone.today - 3.months
            when 4
                date = Time.zone.today - 6.months
            when 5
                date = Time.zone.today - 1.year
            end
            events = Event.where("cast(scheduled as date) >= cast(? as date) AND position_id IN (?)", date.to_date, ids).order(scheduled: :desc)
        elsif type == "manual"
            if !settings.manual_from_date.blank? && !settings.manual_to_date.blank?
                events = Event.where("cast(scheduled as date) BETWEEN cast(? as date) AND cast(? as date) AND position_id IN (?)",Time.zone.parse(settings.manual_from_date.to_date.to_s),Time.zone.parse(settings.manual_to_date.to_date.to_s), ids)
            elsif !settings.manual_from_date.blank?
                events = Event.where("cast(scheduled as date) >= cast(? as date) AND position_id IN (?)",Time.zone.parse(settings.manual_from_date.to_date.to_s), ids)
            elsif !settings.manual_to_date.blank?
                events = Event.where("cast(scheduled as date)  <= cast(? as date) AND position_id IN (?)",Time.zone.parse(settings.manual_to_date.to_date.to_s), ids)
            end
        else
            events = ""
        end
        events
    end

    def get_date_title(type, settings)
        if type == "auto"
            case settings.auto_frecuence
            when 1
                date_title = " durante el último mes"
            when 2
                date_title = " durante los últimos dos meses"
            when 3
                date_title = " durante los últimos tres meses"
            when 4
                date_title = " durante los últimos seis meses"
            when 5
                date_title = " durante el último año"
            end
        elsif type == "manual"
            if !settings.manual_from_date.blank? && !settings.manual_to_date.blank?
                date_title = " desde el #{Time.zone.parse(settings.manual_from_date.to_date.to_s).strftime("%d/%m/%Y")}, hasta el #{Time.zone.parse(settings.manual_to_date.to_date.to_s).strftime("%d/%m/%Y")}"
            elsif !settings.manual_from_date.blank?
                date_title = " desde el #{Time.zone.parse(settings.manual_from_date.to_date.to_s).strftime("%d/%m/%Y")}"
            elsif !settings.manual_to_date.blank?
                date_title = " hasta el #{Time.zone.parse(settings.manual_to_date.to_date.to_s).strftime("%d/%m/%Y")}"
            end
        end
        date_title
    end
end