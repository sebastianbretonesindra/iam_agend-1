require 'organization_exporter'
require 'fileutils'

namespace :export do
  desc "Exports organizations to public/export/lobbies.csv,
        public/export/lobbies.xlsx and public/export/lobbies.json"
  task organizations: :environment do
    folder = Rails.root.join('public', 'export')
    FileUtils.mkdir_p folder unless Dir.exist?(folder)

    exporter = OrganizationExporter.new

    exporter.save_csv(folder.join('lobbies.csv'))
    puts "lobbies.csv saved to public/export/lobbies.csv ✅"
    exporter.save_xlsx(folder.join('lobbies.xlsx'))
    puts "lobbies.xlsx saved to public/export/lobbies.xlsx ✅"
    exporter.save_json(folder.join('lobbies.json'))
    puts "lobbies.json saved to public/export/lobbies.json ✅"
  end

  desc "Exports organizations' events to public/export/agendas.csv,
        public/export/agendas.xlsx and public/export/agendas.json"
  task agendas: :environment do
    folder = Rails.root.join('public', 'export')
    FileUtils.mkdir_p folder unless Dir.exist?(folder)

    exporter = EventsExporter.new true

    exporter.save_csv(folder.join('agendas.csv'))
    puts "agendas.csv saved to public/export/agendas.csv ✅"
    exporter.save_xlsx(folder.join('agendas.xlsx'))
    puts "agendas.xlsx saved to public/export/agendas.xlsx ✅"
    exporter.save_json(folder.join('agendas.json'))
    puts "agendas.json saved to public/export/agendas.json ✅"
  end
end
