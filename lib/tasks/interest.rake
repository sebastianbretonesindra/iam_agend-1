namespace :interest do
    desc "Update interests"
    task update: :environment do
        count = 0
        Interest.all.each do |interest|
            exist = false
            [
                'Actividad económica y empresarial',
                'Distritos',
                'Administración de personal y recursos humanos',
                'Administración electrónica',
                'Administración económica, financiera y tributaria de la Ciudad',
                'Atención a la ciudadanía',
                'Comercio',
                'Consumo',
                'Cultura (bibliotecas, archivos, museos, patrimonio histórico artístico, etc.)',
                'Deportes',
                'Desarrollo tecnológico',
                'Educación y Juventud',
                'Emergencias y seguridad',
                'Empleo',
                'Medio Ambiente',
                'Medios de comunicación',
                'Movilidad, transporte y aparcamientos',
                'Salud',
                'Servicios sociales',
                'Transparencia y participación ciudadana',
                'Turismo',
                'Urbanismo',
                'Vivienda',
                'Otros'
            ].each do |name|
                if interest.name.to_s == name
                    exist = true
                end
            end
            if exist == false
                count = count + 1
                interest.destroy
            end
        end
        puts "=========================================================="
        puts "Se han eliminado #{count} áreas de interés duplicadas."
        puts "=========================================================="
    end

    desc "Add new areas"
    task add_area: :environment do
        interest = Interest.new(id: 0, name: "")
        if interest.save
            puts "======================================="
            puts "Área de interés de voluntariado creada."
            puts "======================================="
        else
            puts "======================================="
            puts interest.errors.full_messages
            puts "======================================="
        end
    end

    desc "Rellenar código de Interest"
    task insert_code: :environment do 
        arr_name = ['Actividad económica y empresarial','Administración de personal y recursos humanos','Administración económica, financiera y tributaria de la Ciudad',
            'Administración electrónica','Atención a la ciudadanía','Comercio','Consumo','Contratación','Cultura (bibliotecas, archivos, museos, patrimonio histórico artístico, etc.)',
            'Deportes','Desarrollo empresarial y emprendimiento','Desarrollo tecnológico','Distritos','Educación y Juventud','Emergencias, seguridad y protección civil','Empleo',
            'Medio ambiente y sostenibilidad ambiental','Medios de comunicación','Movilidad, transporte y aparcamientos','Obras e infraestructuras','Otros','Patrimonio e inventario',
            'Política social de vivienda','Salud','Servicios sociales','Transparencia y participación ciudadana','Turismo','Urbanismo','Vivienda','Voluntariado']

        arr_name.each_with_index do |name,index| 
            Interest.find_by(name: name)&.update(code: (index+1))
        end
    end
end