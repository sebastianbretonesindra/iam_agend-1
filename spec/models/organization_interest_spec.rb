require 'rails_helper'

RSpec.describe OrganizationInterest, type: :model do

  #let!(:newsletters_interest) { build(:newsletters_interest)}

  it "Should be valid" do
    organization = build(:organization)
    interest = create(:interest, name: "Otros", code: "other")
    organization_interest = OrganizationInterest.new(interest: interest, organization: organization)
    
    organization.interests << interest
    organization.save
    
    expect(organization_interest).to be_valid
  end

 
end
