require 'rails_helper'

RSpec.describe Address, type: :model do
  let(:address) { build(:address) }

  it "should not be valid without title defined" do
    expect(address.valid?).to be true
  end

  it "full_address" do
    expect(address.full_address).not_to eq(nil)
  end

  it "full_number" do
    expect(address.full_number).not_to eq(nil)
  end

  
end
