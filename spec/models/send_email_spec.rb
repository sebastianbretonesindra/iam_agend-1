require 'rails_helper'

RSpec.describe SendEmail, type: :model do
  let(:send_email) { build(:send_email) }

  it "should be valid automatize" do
    expect(send_email).to be_valid
  end

  it "should be valid manual" do
    send_email.manual_send=true
    
    expect(send_email).to be_valid
  end


  
end
