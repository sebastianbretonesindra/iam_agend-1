require 'rails_helper'

describe LegalRepresentant do

  let(:legal_representant) { build(:legal_representant) }

  it "should have a correct fullname" do
    fullname = "#{legal_representant.name} #{legal_representant.first_surname} #{legal_representant.second_surname}"

    expect(legal_representant.fullname).to eq(fullname)
  end

  it "should be valid" do
    expect(legal_representant).to be_valid
  end

  describe "#fullname" do
    it "Should return first_surname and second_surname when they are defined" do
      legal_representant.name = "Name"
      legal_representant.first_surname = ""
      legal_representant.second_surname = ""

      expect(legal_representant.fullname).to eq "Name"
    end

    it "Should return first_surname and second_surname when they are defined" do
      legal_representant.name = "Name"
      legal_representant.first_surname = "FirstSurname"
      legal_representant.second_surname = ""

      expect(legal_representant.fullname).to eq "Name FirstSurname"
    end

    it "Should return first_surname and second_surname when they are defined" do
      legal_representant.name = "Name"
      legal_representant.first_surname = "FirstSurname"
      legal_representant.second_surname = "SecondSurname"

      expect(legal_representant.fullname).to eq "Name FirstSurname SecondSurname"
    end
  end

  it "fullname bussiness" do
    legal_representant.business_name = "xxxxxx"
    expect(legal_representant.fullname).not_to eq("")
  end

  it "document_identifier" do
    expect(legal_representant.send(:document_identifier)).to eq(nil)
    legal_representant.identifier_type = "DNI/NIF"
    expect(legal_representant.send(:document_identifier)).to eq(nil)

    legal_representant.identifier_type = "NIE"
    expect(legal_representant.send(:document_identifier)).to eq(nil)

    legal_representant.identifier_type = "Pasaporte"
    expect(legal_representant.send(:document_identifier)).to eq(nil)


    legal_representant.identifier_type = nil
    expect(legal_representant.send(:document_identifier)).not_to eq(nil)
  end

  it "name_entity" do
    expect(legal_representant.send(:name_entity)).to eq(nil)

    legal_representant.business_name = "xxxxxx"
    expect(legal_representant.send(:name_entity)).not_to eq(nil)

    legal_representant.business_name = nil
    legal_representant.name = nil
    expect(legal_representant.send(:name_entity)).to eq(nil)
  end

end
