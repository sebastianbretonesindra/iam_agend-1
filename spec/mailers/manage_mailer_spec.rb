require "rails_helper"
RSpec.describe ManageMailer, type: :mailer do

    it "manage_mailer" do
        data_json_cc= "transparenciaydatos@madrid.es"
        data_json_cco ="creaspodhe@madrid.es"


        ManageEmail.create!(type_data: "Holder", sender: "transparenciaydatos@madrid.es",
            fields_cc: data_json_cc, 
            fields_cco: data_json_cco,
            subject: I18n.t('mailers.new_holder.subject'), 
            email_body: I18n.t("mailers.new_holder.body")) 

        ManageEmail.create!(type_data: 'AuxManager', sender: "transparenciaydatos@madrid.es",
            fields_cc: data_json_cc, 
            fields_cco: data_json_cco,
            subject: I18n.t('mailers.new_aux.subject'), 
            email_body: I18n.t("mailers.new_aux.body"))
        expect(ManageMailer.sender("to@example.org", ManageEmail.all.first).deliver_now).not_to eq(nil)
    end
end
