require 'spec_helper'

require "rake"

describe "registered_lobby rake" do
  before do
    Rake.application.rake_require "tasks/registered_lobby"
    Rake::Task.define_task(:environment)
  end

  describe "#insert_code" do
    let :run_rake_task do
      Rake::Task["registered_lobby:insert_code"].reenable
      Rake.application.invoke_task "registered_lobby:insert_code"
    end

    context "insert_code success" do
      it "update code" do
        run_rake_task     
        expect(RegisteredLobby.all.where(code: nil).count).to eq(0)
      end
    end
  end 

end