require 'spec_helper'

describe "email_type_data rake" do
  before do
    Rake.application.rake_require "tasks/bareg"
    Rake::Task.define_task(:environment)
  end

  describe "#iniciar_expediente" do
    let :run_rake_task do
      Rake::Task["bareg:iniciar_expediente"].reenable
      Rake.application.invoke_task "bareg:iniciar_expediente"
    end

    context "iniciar_expediente success" do
      it "iniciar_expediente" do 

        expect(run_rake_task).not_to eq('')
      end
    end
  end

  describe "#insertar_expediente" do
    let :run_rake_task do
      Rake::Task["bareg:insertar_expediente"].reenable
      Rake.application.invoke_task "bareg:insertar_expediente"
    end

    context "insertar_expediente success" do
      it "insertar_expediente" do 

        expect(run_rake_task).not_to eq('')
      end
    end
  end

  describe "#actualizar_expediente" do
    let :run_rake_task do
      Rake::Task["bareg:actualizar_expediente"].reenable
      Rake.application.invoke_task "bareg:actualizar_expediente"
    end

    context "actualizar_expediente success" do
      it "actualizar_expediente" do 

        expect(run_rake_task).not_to eq('')
      end
    end
  end

  describe "#baja_expediente" do
    let :run_rake_task do
      Rake::Task["bareg:baja_expediente"].reenable
      Rake.application.invoke_task "bareg:baja_expediente"
    end

    context "baja_expediente success" do
      it "baja_expediente" do 

        expect(run_rake_task).not_to eq('')
      end
    end
  end
end
