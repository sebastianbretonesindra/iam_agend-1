require 'spec_helper'

describe "import organizations rake" do
  before do
    Rake.application.rake_require "tasks/import_organizations"
    Rake::Task.define_task(:environment)
  end

  describe "#federations" do
    let :run_rake_task do
      Rake::Task["import_organizations:federations"].reenable
      Rake.application.invoke_task "import_organizations:federations"
    end

    context "federations success" do
      it "federations" do
        file_route = Rails.root.join('spec', 'fixtures', 'federations.csv')
        allow(PublicOrganizationImporter).to receive(:get_file).and_return(File.open(file_route).read)
        run_rake_task

       
        federation = Organization.last
        expect(federation.try(:entity_type)).not_to eq('')
      end
    end
  end

  describe "#associations" do
    let :run_rake_task do
      Rake::Task["import_organizations:associations"].reenable
      Rake.application.invoke_task "import_organizations:associations"
    end

    context "associations success" do
      it "associations" do
        file_route = Rails.root.join('spec', 'fixtures', 'associations.csv')
        allow(PublicOrganizationImporter).to receive(:get_file).and_return(File.open(file_route).read)
        run_rake_task

       
        federation = Organization.last
        expect(federation.try(:entity_type)).not_to eq('')
      end
    end
  end
end