require 'faker'

FactoryGirl.define do
  factory :represented_entity do
    identifier { Faker::Number.number(10) }
    name { Faker::Name.first_name }
    first_surname { Faker::Name.last_name }
    second_surname { Faker::Name.last_name }
    from Time.zone.yesterday
    fiscal_year 2018
    range_fund :range_1
    subvention false
    contract true
    association :legal_representant, factory: :legal_representant
    association :category, factory: :category
    association :organization, factory: :organization
    association :address, factory: :address
    identifier_type "a"
  end

end
