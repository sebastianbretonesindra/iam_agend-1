require 'faker'

FactoryGirl.define do
  factory :organizations_renewal do
    renovation_date Time.zone.now - 2.years
    expiration_date Time.zone.now
    active true
    association :organization, factory: :organization
  end
end
