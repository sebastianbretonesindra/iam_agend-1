require 'faker'

FactoryGirl.define do

  factory :agent do
    name { Faker::Name.first_name }
    first_surname { Faker::Name.last_name }
    second_surname { Faker::Name.last_name }
    from Time.zone.yesterday
    public_assignments "text"
    #file_allow_data { File.new(Rails.root.join('spec', 'support', 'fixtures', 'image.jpg')) }
    allow_public_data true
    attachments {build_list :attachment, 1}
    association :organization, factory: :organization
  end

end
