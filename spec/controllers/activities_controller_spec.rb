require 'rails_helper'

RSpec.describe ActivitiesController, type: :controller do
  before(:each) do
    sign_in create(:user)
  end

  describe "GET #index" do
    it 'should inherit behavior from Parent' do
      expect(ActivitiesController.superclass).to eq(AdminController)
    end

    it 'assigns all activities as @activities' do
      get :index
      expect(response.status).to eq(200)
    end
    
  end

end

