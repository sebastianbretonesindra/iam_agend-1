require 'rails_helper'

RSpec.describe Admin::ParametrizationsController, type: :controller do
  before(:each) do
    sign_in create(:user, :admin)
  end
  
  describe "GET #index" do
    it 'status 200' do
      create(:manage_email)
      create(:data_preference,title: "xxxxx")
      get :index
      expect(response.status).to eq(200)
    end
  end

  describe "GET #change_view" do
    it 'status 302' do
      create(:manage_email)
      create(:data_preference,title: "xxxxx")
      get :change_view
      expect(response.status).to eq(302)
    end
  end

  describe "GET #length_event_description" do
    it 'status 302' do
      create(:manage_email)
      create(:data_preference, title: "xxxxx")
      get :length_event_description, data_preference: {"xxxxx" => 0}
      expect(response.status).to eq(302)
    end
  end
  
end

