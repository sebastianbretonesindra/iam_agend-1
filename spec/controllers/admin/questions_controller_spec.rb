require 'rails_helper'

RSpec.describe Admin::QuestionsController, type: :controller do
  before(:each) do
    sign_in create(:user, :admin)
  end
  
  describe "GET #index" do
    it 'status 200' do
     
      get :index
      expect(response.status).to eq(200)
    end
  end

  describe "GET #new" do
    it 'status 200' do
     
      get :new
      expect(response.status).to eq(200)
    end
  end

  describe "GET #edit" do
    it 'status 200' do
      question = create(:question)
      get :edit, id: question.id
      expect(response.status).to eq(200)
    end
  end

  describe "GET #destroy" do
    it 'status 200' do
      question = create(:question)
      get :destroy, id: question.id
      expect(response.status).to eq(302)
    end
  end

  describe "PUT #update" do
    it 'status 302' do
      question = create(:question)
      put :update, {id: question.id, question: question.attributes}
      expect(response.status).to eq(302)
    end
  end

  describe "POST #create" do
    it 'status 302' do
      question = create(:question)
      question.id = nil
      post :create, {question: question.attributes}
      expect(response.status).to eq(200)
    end
  end

  describe "GET #order" do
    it 'status 200' do
      question = create(:question)
      get :order, ordered_list: {}
      expect(response.status).to eq(302)
    end
  end
  
end

