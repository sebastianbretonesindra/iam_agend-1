require 'rails_helper'

RSpec.describe Admin::NotificationsController, type: :controller do
    before(:each) do
        sign_in create(:user, :admin)
    end
      
    it "active" do
        expect(subject.active).to_not eq(nil)
    end

    it "historic" do
        expect(subject.historic).to_not eq(nil)
    end

    it "edit" do
        expect(subject.edit).to_not eq(nil)
    end

    it "update_notification" do
        expect(subject.update_notification).to_not eq(nil)
    end

    it "pre_recover" do
        expect(subject.pre_recover).to eq(nil)
    end

    it "recover" do
        expect(subject.recover).to_not eq(nil)
    end

    it "pre_canceled" do
        expect(subject.pre_canceled).to eq(nil)
    end

    it "canceled" do
        expect(subject.canceled).to_not eq(nil)
    end

    it "permited" do
        expect(subject.permited).to_not eq(nil)
    end

    it "recover_origin" do
        expect(subject.recover_origin).to_not eq(nil)
    end

    it "show" do
        expect(subject.show).to_not eq(nil)
    end

    it "manually_permited" do
        expect(subject.manually_permited).to_not eq(nil)
    end
end