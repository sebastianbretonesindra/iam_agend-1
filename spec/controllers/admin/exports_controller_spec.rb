require 'rails_helper'

RSpec.describe Admin::ExportsController, type: :controller do
  before(:each) do
    sign_in create(:user, :admin)
  end
  
  describe "GET #index" do
    it 'index' do
      create(:export)

      exports= Export.all
      get :index
      expect(assigns(:exports).count).to eq(1)
    end
  end

  describe "GET #new" do
    it 'new' do
      create(:export)

      exports= Export.all
      get :new
      expect(response.status).to eq(200)
    end
  end

  describe "GET #show" do
    

    it 'show csv' do
      export = create(:export)

      get :show, {id: export.id, format: :csv}
      expect(response.status).to eq(200)
    end
  end

  describe "GET #destroy" do
    it 'destroy' do
      export = create(:export)

      get :destroy, id: export.id
      expect(response.status).to eq(302)
    end
  end

  describe "GET #edit" do
    it 'edit' do
      export = create(:export)

      get :edit, id: export.id
      expect(response.status).to eq(200)
    end
  end

  describe "PUT #update" do
    it 'edit' do
      export = create(:export)

      put :update, {id: export.id, export: export.attributes}
      expect(response.status).to eq(302)
    end
  end

  describe "POST #create" do
    it 'create' do
      export = create(:export)
      export.id = nil
      post :create, export: export.attributes
      expect(response.status).to eq(200)
    end

    it 'create with fields' do
      export = create(:export, fields: {scheduled_field: 1})
      export.id = nil
      post :create, export: export.attributes
      expect(response.status).to eq(302)
    end
  end

 
end

