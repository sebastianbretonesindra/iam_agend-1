require 'rails_helper'

RSpec.describe Admin::OrganizationsController, type: :controller do
  before(:each) do
    sign_in create(:user, :admin)
  end
  
  describe "GET #index" do
    it 'status 200' do
      organization = create(:organization)
      get :index
      expect(response.status).to eq(200)
    end

    it 'status order' do
      organization = create(:organization)
      get :index, order: 1
      expect(response.status).to eq(200)

      get :index, order: 2
      expect(response.status).to eq(200)

      get :index, order: 3
      expect(response.status).to eq(200)

      get :index, order: 4
      expect(response.status).to eq(200)
    end

    it 'status status_type' do
      organization = create(:organization)
      get :index, status_type: [1,2,3,4,5]
      expect(response.status).to eq(200)

    end

    it 'status inscription_date' do
      organization = create(:organization)
      get :index, search_inscription_start_date: Time.zone.now
      expect(response.status).to eq(200)

      get :index, search_inscription_end_date: Time.zone.now
      expect(response.status).to eq(200)

      get :index,{search_inscription_start_date: Time.zone.now, search_inscription_end_date: Time.zone.now}
      expect(response.status).to eq(200)

    end

    it 'status renovation_date' do
      organization = create(:organization)
      get :index, search_renovation_start_date: Time.zone.now
      expect(response.status).to eq(200)

      get :index, search_renovation_end_date: Time.zone.now
      expect(response.status).to eq(200)

      get :index,{search_renovation_start_date: Time.zone.now, search_renovation_end_date: Time.zone.now}
      expect(response.status).to eq(200)

    end

    it 'status expired_date' do
      organization = create(:organization)
      get :index, search_expired_start_date: Time.zone.now
      expect(response.status).to eq(200)

      get :index, search_expired_end_date: Time.zone.now
      expect(response.status).to eq(200)

      get :index,{search_expired_start_date: Time.zone.now, search_expired_end_date: Time.zone.now}
      expect(response.status).to eq(200)

    end
    
  end

  describe "GET #show" do
    it 'status 200' do
      organization = create(:organization)
      get :show, id: organization.id
      expect(response.status).to eq(200)
    end

    it 'status 200 pdf' do
      organization = create(:organization)
      get :show, {id: organization.id, format: :pdf}
      expect(response.status).to eq(200)
    end
  end

  describe "GET #new" do
    it 'status 200' do
      organization = create(:organization)
      get :new
      expect(response.status).to eq(200)
    end
  end

  describe "GET #edit" do
    it 'status 200' do
      organization = create(:organization)
      get :edit, id: organization.id
      expect(response.status).to eq(200)
    end
  end

  describe "GET #destroy" do
    it 'status 302' do
      organization = create(:organization)
      get :destroy, id: organization.id
      expect(response.status).to eq(302)
    end
  end

  describe "POST #create" do
    it 'status 302' do
      interest = create(:interest, name: "Otros", code: "other")
      organization = create(:organization, range_fund: nil, renovation_date: Time.zone.now - 1.years)
      organization.id = nil
      post :create, organization: organization.attributes
      expect(response.status).to eq(302)
    end
  end

  describe "PUT #update" do
    it 'status 302' do
      interest = create(:interest, name: "Otros", code: "other")
      organization = create(:organization)
      organization = create(:organization, range_fund: nil,renovation_date: Time.zone.now - 1.years)
      put :update, {id: organization.id, organization: organization.attributes}
      expect(response.status).to eq(302)
    end

    it 'status 302 invalidated' do
      interest = create(:interest, name: "Otros", code: "other")
      organization = create(:organization)
      create(:organizations_renewal, organization: organization, active: true)
      organization = create(:organization, range_fund: nil,renovation_date: Time.zone.now - 1.years,
        invalidated_at: Time.zone.now, invalidated_reasons: "xxxx")
      put :update, {id: organization.id, organization: organization.attributes}
      expect(response.status).to eq(302)
    end
  end

  

end

