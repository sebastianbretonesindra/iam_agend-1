require 'rails_helper'

RSpec.describe StatisticsController, type: :controller do
  before(:each) do
    sign_in create(:user)
  end

  describe "GET #index" do
    it 'success 200' do
      create(:category, new_category: false, display: true)
      new_cat= create(:category, new_category: true, display: true, category_id: nil)
      create(:category, new_category: true, display: true, category_id: new_cat.id)
      create(:category, new_category: true, display: true, category_id: nil)
      get :index
      expect(response.status).to eq(200)
    end
  end

  describe "GET #lobbies" do
    it 'success 200' do
      organization = create(:organization)
      get :lobbies, type: "1"
      expect(response.status).to eq(200)
    end
  end

end

