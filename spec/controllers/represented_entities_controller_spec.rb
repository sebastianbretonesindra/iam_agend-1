require 'rails_helper'

RSpec.describe RepresentedEntitiesController, type: :controller do
  before(:each) do
    sign_in create(:user, :admin)
  end

  describe "GET #index" do
    it 'get json' do
      organization = create(:organization)
      create(:represented_entity, organization: organization)
      get :index, {organization_id: organization.id}
      expect(response).not_to eq(nil)
    end
  end

  
end

