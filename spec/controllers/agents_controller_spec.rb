require 'rails_helper'

RSpec.describe AgentsController, type: :controller do
  before(:each) do
    sign_in create(:user, :admin)
  end

  describe "GET #index" do
    it 'should inherit behavior from Parent' do
      expect(AgentsController.superclass).to eq(AdminController)
    end

    it 'assigns all agents as @agents' do
      organization= create(:organization)
      get :index, organization_id: organization.id
      expect(response.status).to eq(200)
    end
    
  end

end

