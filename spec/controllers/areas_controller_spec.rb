require 'rails_helper'

RSpec.describe AreasController, type: :controller do
  before(:each) do
    sign_in create(:user, :admin)
  end
  
  describe "GET #index" do
    it 'status 200' do
     
      get :index
      expect(response.status).to eq(200)
    end
  end

  describe "GET #new" do
    it 'status 200' do
     
      get :new
      expect(response.status).to eq(200)
    end
  end

  describe "GET #edit" do
    it 'status 200' do
      area = create(:area)
      get :edit, id: area.id
      expect(response.status).to eq(200)
    end
  end

  describe "GET #destroy" do
    it 'status 200' do
      area = create(:area)
      get :destroy, id: area.id
      expect(response.status).to eq(302)
    end
  end

  describe "PUT #update" do
    it 'status 302' do
      area = create(:area)
      put :update, {id: area.id, area: area.attributes}
      expect(response.status).to eq(302)
    end
  end

  describe "POST #create" do
    it 'status 302' do
      area = create(:area)
      area.id = nil
      post :create, {area: area.attributes}
      expect(response.status).to eq(302)
    end
  end

 
  
end

