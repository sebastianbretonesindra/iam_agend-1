require 'rails_helper'

RSpec.describe StatisticsHelper, type: :helper do
    let!(:organization1) { create(:organization, user: create(:user,:lobby) )}
    let!(:lobbies) { Organization.all }   


    it "employee_lobbies_count" do
        expect(helper.employee_lobbies_count(lobbies)).not_to eq(0)
    end

    it "self_employed_lobbies_count" do
        expect(helper.self_employed_lobbies_count(lobbies).count).not_to eq(1)
    end

    it "self_employed_and_employee_lobbies_count" do
        expect(helper.self_employed_and_employee_lobbies_count(lobbies).count).to eq(0)
    end
end