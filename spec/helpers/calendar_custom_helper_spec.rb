require 'rails_helper'

RSpec.describe CalendarCustomHelper, type: :helper do
    it "get_week_first_monday" do   
        expect(helper.get_week_first_monday).not_to eq(nil)      
    end

    it "get_week_first_sunday" do   
        expect(helper.get_week_first_sunday).not_to eq(nil)      
    end

    it "get_week_first_monday_with_hour" do   
        expect(helper.get_week_first_monday_with_hour).not_to eq(nil)      
    end

    it "get_out_day" do  
        expect(helper.send(:get_out_day)).not_to eq(nil)      
    end

    it "get_days_week_range" do  
        expect(helper.get_days_week_range).not_to eq(nil)      
    end

    it "get_days_month_range" do  
        expect(helper.get_days_month_range(Date.today,Date.tomorrow)).not_to eq(nil)      
    end

    it "set_now_calendar_week" do  
        expect(helper.set_now_calendar_week).not_to eq(nil)      
    end

    it "set_now_calendar" do  
        expect(helper.set_now_calendar).not_to eq(nil)      
    end

    it "set_next_month" do  
        expect(helper.set_next_month).not_to eq(nil)      
    end

    it "set_previous_month" do  
        expect(helper.set_previous_month).not_to eq(nil)      
    end

    it "set_next_week" do  
        expect(helper.set_next_week).not_to eq(nil)      
    end

    it "set_previous_week" do  
        expect(helper.set_previous_week).not_to eq(nil)      
    end

    it "set_tab_calendar" do  
        expect(helper.set_tab_calendar).not_to eq(nil)      
    end

    it "set_tab_agend" do  
        expect(helper.set_tab_agend).not_to eq(nil)      
    end

    it "set_tab_agend_calendar" do  
        expect(helper.set_tab_agend_calendar).not_to eq(nil)      
    end

    
end