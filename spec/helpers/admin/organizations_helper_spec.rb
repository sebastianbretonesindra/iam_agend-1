require 'rails_helper'

RSpec.describe Admin::OrganizationsHelper, type: :helper do
    let!(:organization) { create(:organization, user: create(:user, :lobby))}

    it "form_new_organization?" do   
        expect(helper.form_new_organization?).not_to eq(nil) 
    end

    it "category_name" do   
        create(:category, id: 1)
        expect(helper.category_name(1)).not_to eq(nil) 
    end

    it "show_partial?" do   
        expect(helper.show_partial?(nil)).to eq(false) 
    end

    it "address_number_types" do   
        expect(helper.address_number_types).to eq([["Km", "Km"], ["Num", "Num"], ["S/N", "S/N"]]) 
    end

    it "option_identifier_types" do   
        expect(helper.option_identifier_types).to eq([["DNI/NIF", "DNI/NIF"], ["NIE", "NIE"], ["NIF", "NIF"], ["Pasaporte", "Pasaporte"]]) 
    end

    it "organization_attachments_download_dropdown_id" do   
        expect(helper.organization_attachments_download_dropdown_id(organization)).not_to eq("") 
    end

    it "options_for_registered_lobby" do   
        expect(helper.options_for_registered_lobby).to eq([]) 
    end
    
end