require 'rails_helper'

RSpec.describe Admin::HolderActiveEventsHelper, type: :helper do
    let!(:position) { Position.create!(title: "xx", start: Time.zone.now, holder: create(:holder), area: create(:area))}

    it "stream_query_rows" do   
        expect(helper.stream_query_rows(Position.all, "xxx")).not_to eq(nil) 
    end

    
end