require 'rails_helper'

RSpec.describe Admin::EventExporterHelper, type: :helper do
    let!(:event) { create(:event)}

    it "stream_query_rows_private" do     
        expect(Event.stream_query_rows_private(Event.all)).not_to eq(nil) 
    end
end