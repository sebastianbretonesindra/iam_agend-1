class ChangeInterestsNewsletters < ActiveRecord::Migration
  def change
    remove_column :newsletters, :interest_ids
    add_column :newsletters, :interest_ids, :jsonb, null: false, default: '{}'
    add_index  :newsletters, :interest_ids, using: :gin
  end
end
