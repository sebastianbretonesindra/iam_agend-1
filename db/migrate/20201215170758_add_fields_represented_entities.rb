class AddFieldsRepresentedEntities < ActiveRecord::Migration
  def change
    add_column :represented_entities, :business_name, :string
    add_column :represented_entities, :check_mail, :boolean
    add_column :represented_entities, :check_sms, :boolean
    add_column :represented_entities, :identifier_type, :string
  end
end
