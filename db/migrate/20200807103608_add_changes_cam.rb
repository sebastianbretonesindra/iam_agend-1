class AddChangesCam < ActiveRecord::Migration
  def change
    add_column :addresses, :movil_phone, :string
    add_reference :represented_entities, :address, :index => true, foreign_key: true
    add_reference :represented_entities, :category, :index => true, foreign_key: true
  end
end
