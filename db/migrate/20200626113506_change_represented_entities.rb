class ChangeRepresentedEntities < ActiveRecord::Migration
  def change
    add_column :represented_entities, :in_group_public_administration, :boolean, :default => false
    add_column :represented_entities, :text_group_public_administration, :text
    add_column :represented_entities, :subvention_public_administration, :boolean, :default => false
    add_column :represented_entities, :contract_turnover, :text
    add_column :represented_entities, :contract_total_budget, :text
    add_column :represented_entities, :contract_breakdown, :text
    add_column :represented_entities, :contract_financing, :text
  end
end
