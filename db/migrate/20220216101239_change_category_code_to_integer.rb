class ChangeCategoryCodeToInteger < ActiveRecord::Migration
  def change
    change_column :categories, :code, 'integer USING CAST(code AS integer)'
  end
end
