class CreateExports < ActiveRecord::Migration
  def change
    create_table :exports do |t|
      t.string :name
      t.timestamps null: false
    end
  end
end
