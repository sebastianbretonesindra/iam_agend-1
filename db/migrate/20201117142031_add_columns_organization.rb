class AddColumnsOrganization < ActiveRecord::Migration
  def change
    add_column :organizations, :business_name, :string
    add_column :organizations, :check_mail, :boolean
    add_column :organizations, :check_sms, :boolean
  end
end
