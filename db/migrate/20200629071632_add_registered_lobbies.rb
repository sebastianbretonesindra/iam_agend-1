class AddRegisteredLobbies < ActiveRecord::Migration
  def change
    execute "INSERT INTO registered_lobbies (name, created_at, updated_at) VALUES ('Comunidad de Madrid', '#{Time.zone.now}','#{Time.zone.now}')"
    execute "INSERT INTO registered_lobbies (name, created_at, updated_at) VALUES ('Castilla-La Mancha', '#{Time.zone.now}','#{Time.zone.now}')"
  end
end
