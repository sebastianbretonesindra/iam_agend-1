class AddColumnKeyEventsAgents < ActiveRecord::Migration
  def change
    add_reference :attachments,:event_agent, index: true
  end
end
