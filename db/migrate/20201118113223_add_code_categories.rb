class AddCodeCategories < ActiveRecord::Migration
  def change
    add_column :organizations, :identifier_type, :string

    [
      {name: "Entidades privadas sin ánimo de lucro", code: "1" },
      {name: "Entidades representativas de intereses colectivos", code: "2"},
      {name: "Agrupaciones de personas que se conformen como plataformas, movimientos, foros o redes ciudadanas sin personalidad jurídica, incluso las constituidas circunstancialmente", code: "3"},
      {name: "Organizaciones empresariales, colegios profesionales y demás entidades representativas de intereses colectivos", code: "4"},
      {name: "Entidades organizadoras de actos sin ánimo lucrativo", code: "5"},
      {name: "Organizaciones no gubernamentales", code: "6"},
      {name: "Grupos de reflexión e instituciones académicas y de investigación", code: "7"},
      {name: "Organizaciones que representan a comunidades religiosas", code: "8"},
      {name: "Organizaciones que representan a autoridades municipales", code: "9"},
      {name: "Organizaciones que representan a autoridades regionales", code: "10"},
      {name: "Organismos públicos o mixtos", code: "11"},
      {name: "Empresas y agrupaciones comerciales, empresariales y profesionales", code: "1"},
      {name: "Consultorías profesionales", code: "2"},
      {name: "Asociaciones comerciales, empresariales y profesionales", code: "3"},
      {name: "Coaliciones y estructuras temporales con fines de lucro", code: "4"},
      {name: "Entidades organizadoras de actos con ánimo de lucro", code: "5"},
      {name: "Cualquier otra entidad con ánimo de lucro", code: "6"},
      
    ].each do |c|
      cat = Category.find_by(name: c[:name])

      if !cat.blank?
        cat.code = c[:code]
        cat.save
      end
    end
  end
end
