class AddColumnsTypeIdentifier < ActiveRecord::Migration
  def change
    add_column :legal_representants, :identifier_type, :string
    add_column :notification_effects, :identifier_type, :string
    add_column :users, :user_key_type, :string
  end
end
