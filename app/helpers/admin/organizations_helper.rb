module Admin
  module OrganizationsHelper
    def form_new_organization?
      params[:controller] == "admin/organizations" &&
        (params[:action] == "new" || params[:action] == "create")
    rescue => e
      begin
        Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def category_name(id)
      return if id.blank?
      Category.find(id).try(:name)
    rescue => e
      begin
        Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def address_number_types
      ["Num", "Km", "S/N"].sort.map { |rl| [rl, rl] }
    rescue => e
      begin
        Rails.logger.error("COD-00003: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def option_identifier_types
      ["DNI/NIF", "NIF", "NIE", "Pasaporte"].sort.map { |rl| [rl, rl] }
    rescue => e
      begin
        Rails.logger.error("COD-00004: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def show_partial?(partial)
      params[:show] ? params[:show] == partial && current_user.lobby? : false
    rescue => e
      begin
        Rails.logger.error("COD-00005: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def organization_attachments_download_dropdown_id(organization)
      "organization_#{organization.try(:id)}_attachments_dropdown"
    rescue => e
      begin
        Rails.logger.error("COD-00006: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def options_for_registered_lobby
      RegisteredLobby.where("name is not null AND name !='' ").order("CASE WHEN name='Ninguno' THEN 1 WHEN name='Otro' THEN 3 ELSE 2 END, name").map { |rl| [rl.name, rl.id] }.uniq
    rescue => e
      begin
        Rails.logger.error("COD-00007: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def options_for_provinces
      Province.where("name is not null AND name !='' ").order(name: :asc).map { |rl| [rl.name, rl.name] }.uniq
    rescue => e
      begin
        Rails.logger.error("COD-00008: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def options_for_countries
      Country.where("name is not null AND name !='' ").order(name: :asc).map { |rl| [rl.name, rl.name] }.uniq
    rescue => e
      begin
        Rails.logger.error("COD-00009: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def options_for_road_types
      RoadType.where("name is not null AND name !='' ").order(name: :asc).map { |rl| [rl.name, rl.name] }.uniq
    rescue => e
      begin
        Rails.logger.error("COD-00010: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def permit_category_with_money(model = nil)
      return false if model.blank?
      model.category.category.name.to_s == "Entidad con ánimo de lucro"
    rescue => e
      begin
        Rails.logger.error("COD-00011: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
      false
    end

    def permit_category_without_money(model = nil)
      return false if model.blank?
      model.category.category.name.to_s == "Entidad sin ánimo de lucro" && model.category.name.to_s != "Agrupaciones de personas que se conformen como plataformas, movimientos, foros o redes ciudadanas sin personalidad jurídica, incluso las constituidas circunstancialmente"
    rescue => e
      begin
        Rails.logger.error("COD-00012: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
      false
    end

  end
end
