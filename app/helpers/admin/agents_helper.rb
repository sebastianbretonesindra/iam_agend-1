module Admin
  module AgentsHelper
    def agent_attachment_persisted_data(attachment)
      return "data-persisted=true" if attachment.persisted?
    rescue => e
      begin
        Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def agent_attachment_persisted_remove_notice(attachment)
      return if attachment.blank?
      render partial: 'layouts/admin_messages',
             locals: {
               alert: t("backend.agents.attachment_destroy_notice")
             }
    rescue => e
      begin
        Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def agents_public_assignments_editor(agent)
      agent.new_record? ? "mceNoEditor" : "tinymce"
    rescue => e
      begin
        Rails.logger.error("COD-00003: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end
  end
end
