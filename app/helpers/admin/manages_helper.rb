module Admin::ManagesHelper
    extend ActiveSupport::Concern 
    def stream_query_rows(export=nil, url)
        ids = export.map(&:id).to_s.gsub(/\[|\]/,"") if !export.blank?
        
        query = "COPY (Select distinct 
        COALESCE(UPPER(CONCAT(holders.last_name,', ',holders.first_name)),'-') as \"#{I18n.t('backend.holders_active.title')}\",
        COALESCE(CAST(areas.title as varchar),'-') as \"#{I18n.t('backend.holder_csv.cod_area')}\",
        COALESCE(CAST(to_char(positions.start,'dd/MM/yyyy') as varchar),'-') as \"#{I18n.t('backend.holders_active.start_date')}\",
        COALESCE(CAST(to_char(positions.to,'dd/MM/yyyy') as varchar),'-') as \"#{I18n.t('backend.holders_active.end_date')}\",
        COALESCE((SELECT users.email FROM users WHERE users.user_key = holders.user_key),'-') as \"#{I18n.t('backend.holder_csv.email')}\",
        COALESCE((SELECT UPPER(CONCAT(users.last_name,', ',users.first_name)) FROM users WHERE manages.user_id = users.id),'-') as \"#{I18n.t('backend.holder_csv.email')}\"    
        FROM holders, areas, positions
        WHERE holders.id = manages.holder_id AND positions.holder_id = manages.holder_id AND areas.id = positions.area_id
        #{"AND manages.id IN (#{ids.to_s.gsub(/\[|\]/,'')})" unless ids.blank?}
        ) TO STDOUT (delimiter ';', FORMAT CSV,HEADER );"
        Admin::StreamingHelper.execute_stream_query(query).each { |x| yield x if block_given? } 
    rescue => e
        begin
          Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
    end
end 