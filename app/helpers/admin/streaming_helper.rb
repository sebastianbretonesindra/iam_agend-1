module Admin::StreamingHelper
    extend ActiveSupport::Concern

   
    def self.execute_stream_query(query)
        aux = []
        conn = ActiveRecord::Base.connection.raw_connection
        conn.copy_data "#{query}" do
            while row = conn.get_copy_data          
              aux.push(row.force_encoding("UTF-8").encode!("ISO-8859-1", {invalid: :replace, undef: :replace, replace: ''}))
            end
        end 

        aux
    rescue => e
        begin
          Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
        []
    end

    def self.transform_html_data(data)
        aux_out = data
        [
            ['&Agrave','À'], ['&Egrave','È'], ['&Igrave','Ì'], ['&Ograve','Ò'], ['&Ugrave','Ù'], ['&agrave','à'], ['&egrave','è'], ['&igrave','ì'], ['&ograve','ò'], ['&ugrave','ù'],
            ['&Aacute','Á'], ['&Eacute','É'], ['&Iacute','Í'], ['&Oacute','Ó'], ['&Uacute','Ú'], ['&Yacute','Ý'], ['&aacute', 'á'], ['&eacute','é'], ['&iacute','í'], ['&oacute','ó'],
            ['&uacute','ú'], ['&yacute','ý'], ['&Acirc', 'Â'], ['&Ecirc', 'Ê'], ['&Icirc', 'Î'], ['&Ocirc', 'Ô'], ['&Ucirc', 'Û'], ['&acirc', 'â'], ['&ecirc', 'ê'], ['&icirc', 'î'],
            ['&ocirc', 'ô'], ['&ucirc', 'û'], ['&Atilde','Ã'], ['&Ntilde','Ñ'], ['&Otilde', 'Õ'], ['&atilde', 'ã'], ['&ntilde','ñ'], ['&otilde','õ'],
            ['&Auml','Ä'], ['&Euml','Ë'],['&Iuml','Ï'], ['&Ouml','Ö'], ['&Uuml', 'Ü'], ['&#159','Ÿ'], ['&auml','ä'], ['&euml', 'ë'], ['&iuml', 'ï'], ['&ouml', 'ö'], ['&uuml','ü'], ['&Yuml','Ÿ'],
            ['&iexcl', '¡'],['&iquest','¿'],['&Ccedil','Ç'],['&ccedil','ç'],['&#140','Œ'],['&#156','œ'],['&szlig','ß'],
            ['&oelig','œ'],['&OElig','Œ'],['&Oslash','Ø'],['&oslash','ø'],['&Aring','Å'],['&aring','å'],['&AElig','Æ'],['&aelig','æ'],
            ['&THORN', 'Þ'], ['&thorn','þ'],['&ETH','Ð'],['&eth','ð'],['&laquo','«'],['&raquo','»'],
            ['&cent','¢'],['&pound','£'],['&yen','¥'],['&euro','€'],['&curren','¤'],
            ['&nbsp',' '], ['&amp', '&'], ['&quot', '\"'], ['&copy', '©'], ['&reg', '®'],
            ['&#153','™'],['&trade','™'],['&para','¶'],['&#149','•'],['&#183','·'],['&sect','§'],['&#150','–'],['&ndash','–'],['&#151','—'],['&mdash','—'],
            ['&gt','>'],['&lt','<'],['&divide','÷'],['&deg','°'],['&not','¬'],['&plusmn','±'],['&micro','µ'],['&brvbar','¦'],['&uml','¨'],['&ordf','ª'],
            ['&shy',' '],['&macr','¯'], ['&sup2','²'],['&sup3','³'],['&acute','´'],['&middot','·'],['&cedil','¸'],['&sup1','¹'],['&ordm','º'],['&frac14','¼'],['&frac12','½'],['&frac34','¾']
        ].each do |s|
            aux_out = "replace(#{aux_out}, '#{s[0]}','#{s[1]}')"
        end
        "COALESCE((Select regexp_replace(#{aux_out}, '<[^>]*>|[;]*','','g')), '-')"
    rescue => e
        begin
          Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
        rescue
        end
        data
    end
end