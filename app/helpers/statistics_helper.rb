module StatisticsHelper
  def employee_lobbies_count(lobbies)
    lobbies.select { |l| l if l.self_employed_lobby && !l.employee_lobby }
  rescue => e
    begin
      Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def self_employed_lobbies_count(lobbies)
    lobbies.select { |l| l if !l.self_employed_lobby && l.employee_lobby}
  rescue => e
    begin
      Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def self_employed_and_employee_lobbies_count(lobbies)
    lobbies.select { |l| l if l.self_employed_lobby && l.employee_lobby }
  rescue => e
    begin
      Rails.logger.error("COD-00003: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end
end
