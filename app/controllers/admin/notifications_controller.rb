class Admin::NotificationsController < AdminController
    load_and_authorize_resource

    before_action :load_notification, except: [:active, :historic]


    def active
      @notifications_unpagined = search(params).pending_view
      @notifications = @notifications_unpagined.page(params[:page]).per(100)

      respond_to do |format|
        format.csv
        format.html
      end
    rescue => e
      begin
        Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def historic
      @notifications_unpagined = search(params).historic_view
      @notifications = @notifications_unpagined.page(params[:page]).per(100)

      respond_to do |format|
        format.csv
        format.html
      end
    rescue => e
      begin
        Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def edit
      @legend_title = t('backend.faq.edit_notification_legend')
      @organization = @notification.saveInicioExpediente(@notification)
      @years=[]
        @this_year = Time.current.year
        (2000..@this_year+5).each do |year|
          @years.push([year,year])
        end
    rescue => e
      begin
        Rails.logger.error("COD-00003: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def update_notification
      @notification.assign_attributes(notification_params)
      @notification.modification_data[:organization] = organization_params
      if @notification.type_action == "Renovación"
        @notification.modification_data[:organization][:renovation_date] = @notification.created_at
      elsif @notification.type_action == "Alta"
        @notification.modification_data[:organization][:inscription_date] = @notification.created_at
        @notification.modification_data[:organization][:renovation_date] = ""
        @notification.modification_data[:organization][:canceled_at] = ""
      end
      if @notification.update_attributes(notification_params)
        redirect_to active_admin_notifications_path, notice: t('backend.notification_alerts.update_success')
      else
        flash[:alert] = t('backend.notification_alerts.update_error')
        render :edit
      end
    rescue => e
      begin
        Rails.logger.error("COD-00004: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def pre_recover
    rescue => e
      begin
        Rails.logger.error("COD-00005: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def recover
      @notification.status = Notification.get_statuses[:recover]
      @notification.change_status_at = Time.zone.now
      @notification.recover_reasons = notification_recover_params[:recover_reasons]
      if @notification.save
        redirect_to active_admin_notifications_path, notice: t('backend.notification_alerts.recover_success')
      else
        flash[:alert] = t('backend.notification_alerts.recover_error')
        redirect_to active_admin_notifications_path
      end
    rescue => e
      begin
        Rails.logger.error("COD-00006: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def pre_canceled
    rescue => e
      begin
        Rails.logger.error("COD-00007: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def canceled
      @notification.status = Notification.get_statuses[:reject]
      @notification.change_status_at = Time.zone.now
      @notification.reject_reasons = notification_reject_params[:reject_reasons]

      if @notification.save
        redirect_to historic_admin_notifications_path, notice: t('backend.notification_alerts.canceled_success')
      else
        flash[:alert] = t('backend.notification_alerts.canceled_error')
        redirect_to historic_admin_notifications_path
      end
    rescue => e
      begin
        Rails.logger.error("COD-00008: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def permited
      @organization = @notification.saveInicioExpediente(@notification)
      Notification.set_errors_detected(@notification)
      
      if !Organization.find_by(identifier: @organization.identifier).try(:id).blank? && @notification.type_action.to_s == I18n.t('backend.notification_action.insert')
        flash[:alert] = t('backend.notification_alerts.permited_error_action')
        redirect_to active_admin_notifications_path
      elsif !@notification.validations_description.blank?
        flash[:alert] = t('backend.notification_alerts.permited_error_errors')
        redirect_to active_admin_notifications_path
      else

        @notification.status = Notification.get_statuses[:permit]
        @notification.change_status_at = Time.zone.now
        @notification.reject_reasons = nil
        @notification.recover_reasons = nil

        change_contact = false
        unless @organization.user.blank?
          change_contact = true if !@organization.try(:user).try(:email).blank? && @organization.user.email.to_s != Organization.find_by(identifier: @organization.identifier).try(:email)
        end

        organization_origin = Organization.find_by(identifier: @organization.identifier)
        if !organization_origin.blank?
          @organization = @notification.saveInicioExpedienteWith(organization_origin, @notification)
        end
        @organization.user.role=2

        if @organization.save(validate: false) && @notification.save
          if !@organization.renovation_date.blank? && @organization.organizations_renewals.find_by(renovation_date: @organization.renovation_date).blank?
            @organization.organizations_renewals.each do |ren|
              ren.active = false
              ren.save
            end
            renewal= OrganizationsRenewal.new(organization: @organization,renovation_date: @organization.renovation_date, expiration_date: @organization.expired_date, active: true)
            @organization.organizations_renewals << renewal
            renewal.save
            @organization.save
          end

          #Alta
          if @notification.type_action.to_s == I18n.t('backend.notification_action.insert')
            date = I18n.l(@organization.try(:inscription_date), format: :short).to_s
            mail = ManageEmail.find_by(type_data: 'Organizations')
            if mail.blank?
              mail = ManageEmail.create!(type_data: 'Organizations', sender: "no-reply@madrid.es",
                fields_cc: "", 
                fields_cco: "registrodelobbies@madrid.es",
                subject: I18n.t('mailers.organizations.subject'), 
                email_body: I18n.t("mailers.organizations.body"))
            end
            mail.subject = mail.subject.gsub!("@organization@", @organization.try(:fullname))
            mail.email_body = mail.try(:email_body).blank? ? "" : mail.try(:email_body).to_s.gsub("@date@", date.blank? ? I18n.l(Time.zone.now, format: :short).to_s : date)
            .gsub("@organization@", @organization.try(:fullname).to_s)
            .gsub("@code@", @organization.try(:id).to_s)
            .gsub("@user@", @organization.try(:user).try(:email).to_s)
            .gsub("@password@", @organization.try(:user).try(:password).to_s)

            begin
              ManageMailer.sender(@organization.try(:user).try(:email), mail).deliver_now
            rescue =>e
              begin
                Rails.logger.error("SMS-ERROR: #{e}")
              rescue
              end
            end
          else
            #resto
            if change_contact
              @organization.user.password = Faker::Internet.password(8)
              @organization.user.save
              begin
                OrganizationMailer.update_contact(@organization.user).deliver_now 
              rescue =>e
                begin
                  Rails.logger.error("SMS-ERROR: #{e}")
                rescue
                end
              end
            elsif !@organization.invalidated? && !@organization.canceled?
              begin 
                OrganizationMailer.update(@organization).deliver_now
              rescue =>e
                begin
                  Rails.logger.error("SMS-ERROR: #{e}")
                rescue
                end
              end
            end
            
            if @organization.invalidated? && params[:organization][:invalidate]
              begin
                OrganizationMailer.invalidate(@organization).deliver_now
              rescue =>e
                begin
                  Rails.logger.error("SMS-ERROR: #{e}")
                rescue
                end
              end
            end
          end

          redirect_to active_admin_notifications_path, notice: t('backend.notification_alerts.permited_success')
        else
          flash[:alert] = t('backend.notification_alerts.permited_error')
          redirect_to active_admin_notifications_path
        end
       
      end
    rescue => e
      begin
        Rails.logger.error("COD-00009: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def recover_origin
      @notification.modification_data = @notification.origin_data
      if @notification.save
        redirect_to active_admin_notifications_path, notice: t('backend.notification_alerts.recover_origin_success')
      else
        flash[:alert] = t('backend.notification_alerts.recover_origin_error')
        redirect_to active_admin_notifications_path
      end
    rescue => e
      begin
        Rails.logger.error("COD-00010: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    def show
      @organization = @notification.saveInicioExpediente(@notification)  
    rescue => e
      begin
        Rails.logger.error("COD-00011: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end    
    end

    def manually_permited
      @notification.status = Notification.get_statuses[:manual_permit]
      @notification.change_status_at = Time.zone.now

      if @notification.save
        redirect_to historic_admin_notifications_path, notice: t('backend.notification_alerts.manually_permited_success')
      else
        flash[:alert] = t('backend.notification_alerts.manually_permited_error')
        redirect_to historic_admin_notifications_path
      end
    rescue => e
      begin
        Rails.logger.error("COD-00012: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
      rescue
      end
    end

    private

    def load_notification
      @notification = Notification.find(params[:id])
      Notification.set_errors_detected(@notification)
    end

    def organization_params
      params.require(:organization)
            .permit(:reference, :identifier,:identifier_type,:business_name, :name, :first_surname, :second_surname,
                    :invalidate, :invalidated_reasons, :renovation_date,
                    :in_group_public_administration, :text_group_public_administration,
                    :subvention_public_administration, :contract_turnover, :contract_total_budget,
                    :contract_breakdown, :contract_financing, :public_term, :communication_term,
                    :web, :validate, :description, :category_id, :other_term,
                    :fiscal_year, :range_fund, :subvention, :contract,
                    :certain_term, :code_of_conduct_term, :gift_term, :lobby_term,
                    :inscription_date, :modification_date, :canceled_at,
                    :own_lobby_activity, :foreign_lobby_activity, :other_registered_lobby_desc,                   
                    address_attributes, legal_representant_attributes,
                    {notification_effect_attributes: [:identifier, :identifier_type,:business_name, :name, :first_surname, :second_surname, :_destroy, address_attributes]},
                    {user_attributes: [:first_name, :last_name, :second_last_name, :role, :email, :active, :phones, :movil_phone,
                      :password, :password_confirmation]},                     
                    {interest_ids: []}, {registered_lobby_ids: []},
                    {range_subventions_attributes: [ :import, :entity_name,:_destroy]},
                    {attachments_attributes: [:file, :_destroy]}, represented_entities_attributes)
    end

    def address_attributes
      {address_attributes: [:country, :province, :town, :address_type, :address, :number, :gateway, :stairs,:floor,
        :door,:postal_code, :email, :phones, :movil_phone, :address_number_type]}
    end

    def legal_representant_attributes
      {legal_representant_attributes: [:identifier,:name,:identifier_type,:business_name, :first_surname, :second_surname, :_destroy,
        address_attributes]}
    end

    def represented_entities_attributes
      if params[:organization][:foreign_lobby_activity].to_s == 'true'
        return {represented_entities_attributes: [:in_group_public_administration, :text_group_public_administration, 
          :subvention_public_administration, :contract_turnover, :contract_total_budget, :contract_breakdown,
          :contract_financing, :identifier,:identifier_type,:business_name, :name, :first_surname, :second_surname, :to,
          :from, :fiscal_year, :range_fund, :subvention, :contract, :category_id,
          address_attributes, legal_representant_attributes,
          {represented_range_subventions_attributes: [:import, :entity_name,:_destroy]}, :_destroy]}
      else
        return {}
      end
    end

    def notification_params
      params.permit(:observations, :type_anotation)
    end

    def notification_recover_params
      params.permit(:recover_reasons)
    end

    def notification_reject_params
      params.permit(:reject_reasons)
    end


    def search(parametrize)
      notifications = Notification.all

      begin
        if !parametrize[:search_id].blank?
          notifications= notifications.where("notifications.id = ?", parametrize[:search_id])
        end
      rescue
      end

      begin
        if !parametrize[:search_type].blank?
          notifications= notifications.where("notifications.type_anotation = ?", parametrize[:search_type])
        end
      rescue
      end

      begin
        if !parametrize[:errors_type].blank?
          if parametrize[:errors_type].to_s == I18n.t("backend.notification_filters.with_errors").to_s
            notifications= notifications.where("notifications.validations_description is not null AND notifications.validations_description != ''")
          elsif parametrize[:errors_type].to_s == I18n.t("backend.notification_filters.without_errors").to_s
            notifications= notifications.where("notifications.validations_description is null OR notifications.validations_description = ''")
          end
        end
      rescue
      end

      begin
        if !parametrize[:search_status].blank?
          notifications= notifications.where("notifications.status = ?", parametrize[:search_status])
        end
      rescue
      end

      begin
        if parametrize[:search_create_start_date].present? && parametrize[:search_create_end_date].present? && Time.zone.parse(parametrize[:search_create_start_date]) >= Time.zone.parse(parametrize[:search_create_end_date])
          notifications = notifications.where("notifications.created_at BETWEEN ?  AND ?",Time.zone.parse(parametrize[:search_create_start_date]),Time.zone.parse(parametrize[:search_create_end_date]))
        elsif parametrize[:search_create_start_date].present?
          notifications = notifications.where("notifications.created_at >= ? ",Time.zone.parse(parametrize[:search_create_start_date]))
        elsif parametrize[:search_create_end_date].present?
          notifications = notifications.where("notifications.created_at  <= ? ",Time.zone.parse(parametrize[:search_create_end_date]))
        end
      rescue
      end
      
      begin
        if parametrize[:search_status_start_date].present? && parametrize[:search_status_end_date].present? && Time.zone.parse(parametrize[:search_status_start_date]) >= Time.zone.parse(parametrize[:search_status_end_date])
          notifications = notifications.where("notifications.change_status_at BETWEEN ?  AND ?",Time.zone.parse(parametrize[:search_status_start_date]),Time.zone.parse(parametrize[:search_status_end_date]))
        elsif parametrize[:search_status_start_date].present?
          notifications = notifications.where("notifications.change_status_at >= ? ",Time.zone.parse(parametrize[:search_status_start_date]))
        elsif parametrize[:search_status_end_date].present?
          notifications = notifications.where("notifications.change_status_at  <= ? ",Time.zone.parse(parametrize[:search_status_end_date]))
        end
      rescue
      end

      begin
        if !parametrize[:search_full_name].blank?
          notifications= notifications.where("notifications.id in (?)", notifications.select {|notification| I18n.transliterate(notification.full_name.downcase.to_s).include?(I18n.transliterate(parametrize[:search_full_name].downcase.to_s))}.map {|n| n.id})
        end
      rescue
      end

      notifications
    end

end
