module Admin
    class ManageEmailsController < AdminController

        load_and_authorize_resource

        # def index
        #     @mails = ManageEmail.all.order(id: :desc)
        # end
        
        # def new
            
        # end

        # def create
            
        # end

        # def show
            
        # end

        def edit
            @mail = ManageEmail.find(params[:id])
            @users = {"Titulares" => 1, "Gestores" => 2, "Ambos" => 3}
        rescue => e
            begin
              Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
            rescue
            end
        end
      
        def update
            @mail = ManageEmail.find(params[:id])
            if @mail.update_attributes(mail_params)
                redirect_to admin_parametrizations_path, notice: t('backend.successfully_created_record')
            else
                flash[:alert] = t('backend.review_errors')
                render :edit
            end        
        rescue => e
            begin
              Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
            rescue
            end
        end
        

        private

            def mail_params
                params.require(:manage_email).permit(:id, 
                    :sender, 
                    :fields_cc, 
                    :fields_cco,
                    :subject,
                    :email_body,
                    :excluded_emails,
                    :users
                )
            end
    end
end
