class HoldersController < AdminController
  load_and_authorize_resource
  before_action :set_holder, only: [:show, :edit, :update, :destroy]
  before_action :load_areas, except: [:download]
  before_action :load_manages, except: [:download]
  before_action :set_manages, only: [:index, :export_manages, :export_all]
  #before_action :get_user_key, only: [:create]
  
  def index
    @holder_unpaginated =  filter(params)  
    @holders = @holder_unpaginated.page( params[:page] || 1).per(300)
  rescue => e
    begin
      Rails.logger.error("COD-00001: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def active
    @positions_unpaginated = filter_active(params).order("holders.last_name,holders.first_name").group(["positions.id","holders.last_name","holders.first_name"])
    @count =  0
    @positions_unpaginated.each {|k,v| @count = @count + 1 }
    @positions = @positions_unpaginated.page( params[:page] || 1).per(300)
    respond_to do |format|
        format.csv { stream_csv_report.delay  }
        format.html
    end
  rescue => e
    begin
      Rails.logger.error("COD-00002: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def show
    @histories = HolderStatusHistory.joins(:position).where("positions.holder_id = #{@holder.id}").order("holder_status_histories.status_at DESC")
  rescue => e
    begin
      Rails.logger.error("COD-00003: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def new
  rescue => e
    begin
      Rails.logger.error("COD-00004: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def edit
  rescue => e
    begin
      Rails.logger.error("COD-00005: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def create
    @holder = Holder.new(holder_params)
    if @holder.save
      uweb = UwebUpdateApi.new
      uweb.insert_profile_user(Rails.application.secrets.uweb_api_holders_key, current_user.user_key, @holder.user_key)
      uweb.insert_profile_user(Rails.application.secrets.uweb_api_admins_key, current_user.user_key, @holder.user_key)
      uweb.insert_profile_user(Rails.application.secrets.uweb_api_users_key, current_user.user_key, @holder.user_key)
      redirect_to holders_path(), notice: t('backend.successfully_created_record')
    else
      flash[:alert] = t('backend.review_errors')
      render :new
    end
  rescue => e
    begin
      Rails.logger.error("COD-00006: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def update
    if @holder.update(holder_params)
      redirect_to holders_path, notice: t('backend.successfully_updated_record')
    else
      flash[:alert] = t('backend.review_errors')
      render :edit
    end
  rescue => e
    begin
      Rails.logger.error("COD-00007: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def destroy
    @holder.positions.each do |position|
      position.to = Date.today if position.to.blank?
      status = I18n.t('backend.inactive')
      position.save
    end

    Manage.where(holder_id: @holder.id).each {|m| remove_user(m.user_id) if Manage.where(user_id: id).count < 2 }
    uweb = UwebUpdateApi.new
    uweb.remove_profile_user(Rails.application.secrets.uweb_api_holders_key, current_user.user_key, @holder.user_key)
    uweb.remove_profile_user(Rails.application.secrets.uweb_api_admins_key, current_user.user_key, @holder.user_key)
    uweb.remove_profile_user(Rails.application.secrets.uweb_api_users_key, current_user.user_key, @holder.user_key)
    #@holder.destroy
    redirect_to holders_path, notice: t('backend.successfully_destroyed_record')
  rescue => e
    begin
      Rails.logger.error("COD-00008: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  def mail  #Envío de email para alta de titular de agenda
      if !@holder.blank? 
        if params[:type_mail].to_s == 'titular' && !@holder.user_email.blank?
          @mail = ManageEmail.find_by(type_data: 'Holder')
          if @mail.blank?
            redirect_to holders_path, alert: t('backend.no_mailer')
          else
            data_mail(@mail)
          end
        elsif params[:type_mail].to_s == 'titular' && @holder.user_email.blank?
          redirect_to holders_path, alert: t('backend.no_holder')
        else
          if params[:type_mail].to_s == 'gestor' && !@holder.users.blank?
            unless empty_emails(@holder) 
              @mail = ManageEmail.find_by(type_data: 'AuxManager')
              if @mail.blank?
                redirect_to holders_path, alert: t('backend.no_mailer')
              else
                data_mail(@mail)
              end
            else
              redirect_to holders_path, alert: t('backend.no_gestor')
            end
          else
            redirect_to holders_path, alert: t('backend.no_gestor')
          end
        end
      else
        redirect_to holders_path, alert: t('backend.no_holder')
      end

      if !@mail.blank?
        @type_mail = @mail.type_data.to_s == "Holder" ? "titular" : "gestor"
      else
        @type_mail = nil
      end
  rescue => e
    begin
      Rails.logger.error("COD-00009: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
    redirect_to holders_path, alert: t('backend.no_mailer')
  end

  def send_mail
    mail = ManageEmail.new(mail_params)
    if mail.valid?
      errors = false
      if mail.type_data.to_s == "Holder"
        begin
         ManageMailer.sender(@holder.user_email,mail).deliver_now
        rescue => e
          errors = true
          flash[:alert] = t('backend.review_errors')
        end
      else
        @holder.users.each do |m| 
          begin
            ManageMailer.sender(m.email,mail).deliver_now
            begin
              Rails.logger.error("SMS-ERROR: #{e}")
            rescue
            end
          rescue => e
            errors = true
            flash[:alert] = t('backend.review_errors')
            begin
              Rails.logger.error("SMS-ERROR: #{e}")
            rescue
            end
          end
        end
      end
      if errors
        redirect_to holders_path
      else
        redirect_to holders_path, notice: t('backend.success_email')
      end
    else
      flash[:alert] = t('backend.review_errors')
      @type_mail = mail.type_data.to_s == "Holder" ? "titular" : "gestor"
      @mail = mail
      render :mail
    end
  rescue => e
    begin
      Rails.logger.error("COD-00010: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end
  
  def export_all
    query = @positions_unpaginated
    
    stream_file("Titulares_y_Gestores", "csv") do |stream|
      Holder.holders_stream_query_rows(query, request.base_url) do |row_from_db|
        stream.write row_from_db
      end
    end
  rescue => e
    begin
      Rails.logger.error("COD-00011: [<#{self.class} - #{e.class}: #{e.message}] #{e.backtrace.join("\n")}")
    rescue
    end
  end

  private

  def remove_user(id)
    user = User.find(id)
    if current_user != user
      user.active=0
      if user.save!
        uweb.remove_profile_user(Rails.application.secrets.uweb_api_users_key, current_user.user_key, user.user_key, Rails.application.secrets.uweb_api_users_key)
      end
    end
  end

  def stream_csv_report
    query = @positions_unpaginated
    
    stream_file("Eventos_agendados_titulares", "csv") do |stream|
      Holder.stream_query_rows(query, params, request.base_url) do |row_from_db|
        stream.write row_from_db
      end
    end
  end


  def get_user_key
    user = User.find(params[:user_id].to_i)
    @holder = Holder.new
    unless user.blank?
      if new_holder(user)
        @holder.first_name = user.first_name
        @holder.last_name = user.last_name
        @holder.user_key = user.user_key
      else
        redirect_to admin_users_path, alert: t('backend.is_holder')
      end
    end
  end

  def new_holder(user)
    return false if user.blank? || user.try(:user_key).blank?
    exist = Holder.where(user_key: user.user_key)
    exist.blank?
  end

  def data_mail(mail)
    mail.subject = mail.subject.gsub!("@Holder@", name)
    managers = "<ul>"
    @holder.users.each { |f| managers = managers + "<li>"+f.email_name+"</li>" }
    managers = managers + "</ul>"
    mail.email_body = mail.email_body.gsub("@Holder@", name).gsub("@Managers@", managers)
  end
  def empty_emails(holder)
    empty=true
    holder.users.each do |e|
      if !e.email.blank?
        empty = false
        break
      end
    end
    return empty
  end

  def set_holder_status_history(position, status = nil)
    history = HolderStatusHistory.generateHistory(position, status)
    history.save
  end

  def set_manages 
    manages = User.joins(:manages).all.uniq.order("users.last_name,users.first_name")
    @manages_collection = []
    @manages_collection[@manages_collection.length] = [I18n.t("main.form.nothing"), "null"]
    @manages_collection[@manages_collection.length] = [I18n.t("main.form.some"), "some"]
    manages.each do |manage|
      @manages_collection[@manages_collection.length] = [manage.full_name_comma, manage.id]
    end
    @manages_collection[@manages_collection.length] = [I18n.t("main.form.any"), ""]
  end

  def set_holder
    @holder = Holder.find(params[:id])
  end

  def holder_params
    params.require(:holder).permit(:first_name, :last_name, :id, :user_key,
      {manages_attributes: [:id, :user_id, :_destroy]},
                                   {positions_attributes: [:id, :holder_id, :title, :area_id, :from, :to,:start,:comments, :_destroy]})
  end

  def load_areas
    @areas = Area.area_tree
  end

  def load_manages
   @manages = User.where(role: 0)
  end

  def name
    @holder.first_name + " " + @holder.last_name
  end

  def search(params)
    # Holder.search do 
    #   with(:user_id, params[:search_contributed_by]) if params[:search_contributed_by].present? && params[:search_contributed_by].to_s!="null" && params[:search_contributed_by].to_s!="some"
    #   paginate page: 1, per_page: Holder.all.count
    # end
    ids = []
    holders = Holder.all
    holders = holders.where(id: params[:search_contributed_by]) if params[:search_contributed_by].present? && params[:search_contributed_by].to_s!="null" && params[:search_contributed_by].to_s!="some"
    holders.each do |holder|
      ids << holder.id
    end
    ids
  end

  def filter(params)
    ids = search(params)
    holders = Holder.includes(:users, :areas, :positions, :manages).where("holders.id IN (?)", ids).order(last_name: :asc)
    holders = holders.where("UPPER(first_name) LIKE UPPER(?) OR UPPER(last_name) LIKE UPPER(?) OR UPPER(CONCAT(first_name,' ',last_name)) LIKE UPPER(?)","%#{params[:search_name]}%","%#{params[:search_name]}%","%#{params[:search_name]}%") if params[:search_name].present?
    holders = holders.where("id NOT IN (?)", Manage.all.select("holder_id")) if params[:search_contributed_by].present? && params[:search_contributed_by].to_s=="null"
    holders = holders.where("id IN (?)", Manage.all.select("holder_id")) if params[:search_contributed_by].present? && params[:search_contributed_by].to_s=="some"
    holders = holders.page( params[:page] || 1).per(Holder.all.count)
  end

  

  def filter_active(params)
    positions = Position.joins(:holder)
    positions = positions.where("UPPER(holders.first_name) LIKE UPPER(?) OR UPPER(holders.last_name) LIKE UPPER(?) OR UPPER(CONCAT(holders.first_name,' ',holders.last_name)) LIKE UPPER(?)","%#{params[:search_title]}%","%#{params[:search_title]}%","%#{params[:search_title]}%") if params[:search_title].present?

    if params[:search_start_date].present? && params[:search_end_date].present?
      positions = positions.joins(:events).where("events.scheduled BETWEEN ?  AND ?  AND (events.scheduled IS NULL OR events.scheduled BETWEEN ?  AND ?)",Time.zone.parse(params[:search_start_date]),Time.zone.parse(params[:search_end_date]),Time.zone.parse(params[:search_start_date]),Time.zone.parse(params[:search_end_date]))
    elsif params[:search_start_date].present?
      positions = positions.joins(:events).where("events.scheduled >= ? ",Time.zone.parse(params[:search_start_date]))
    elsif params[:search_end_date].present?
      positions = positions.joins(:events).where("events.scheduled  <= ?  AND (events.scheduled IS NULL OR events.scheduled  <= ?)",Time.zone.parse(params[:search_end_date]),Time.zone.parse(params[:search_end_date]))
    end

    if params[:search_start_start_date].present? && params[:search_end_start_date].present?
      positions = positions.where("positions.start BETWEEN ?  AND ?  AND (positions.start IS NULL OR positions.start BETWEEN ?  AND ?)",Time.zone.parse(params[:search_start_start_date]),Time.zone.parse(params[:search_end_start_date]),Time.zone.parse(params[:search_start_start_date]),Time.zone.parse(params[:search_end_start_date]))
    elsif params[:search_start_start_date].present?
      positions = positions.where("positions.start >= ? ",Time.zone.parse(params[:search_start_start_date]))
    elsif params[:search_end_start_date].present?
      positions = positions.where("positions.start  <= ?  AND (positions.start IS NULL OR positions.start  <= ?)",Time.zone.parse(params[:search_end_start_date]),Time.zone.parse(params[:search_end_start_date]))
    end

    if params[:search_start_end_date].present? && params[:search_end_end_date].present?
      positions = positions.where("positions.to BETWEEN ?  AND ?  AND (positions.to IS NULL OR positions.to BETWEEN ?  AND ?)",Time.zone.parse(params[:search_start_end_date]),Time.zone.parse(params[:search_end_end_date]),Time.zone.parse(params[:search_start_end_date]),Time.zone.parse(params[:search_end_end_date]))
    elsif params[:search_start_end_date].present?
      positions = positions.where("positions.to >= ? ",Time.zone.parse(params[:search_start_end_date]))
    elsif params[:search_end_end_date].present?
      positions = positions.where("positions.to  <= ?  AND (positions.to IS NULL OR positions.to  <= ?)",Time.zone.parse(params[:search_end_end_date]),Time.zone.parse(params[:search_end_end_date]))
    end
    if params[:theme].blank? || params[:theme].to_s == "Activos"
      positions = positions.where(to: nil) 
    end

    positions
  end

  def mail_params
    params.require(:manage_email).permit(:id,
        :type_data, 
        :sender, 
        :fields_cc, 
        :fields_cco,
        :subject,
        :email_body
    )
  end
end
