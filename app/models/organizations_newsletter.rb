class OrganizationsNewsletter < ActiveRecord::Base
    belongs_to :organization
    belongs_to :newsletter

    validate :uniq_send_email


    private

    def uniq_send_email
        orgnew = OrganizationsNewsletter.find_by(newsletter: self.newsletter, organization: self.organization)

        if !orgnew.blank?
            self.errors.add(:organization,"Ya existe un elemento enviado")
        end
    rescue
    end

end
