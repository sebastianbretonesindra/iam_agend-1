class PeticionIniciarExpediente < WashOut::Type
    type_name 'PeticionIniciarExpediente'
    map  codTipoExpediente: :string, 
      xmlDatosEntrada: :string, 
      listDocs: ArrayDocs, 
      listFormatos: ArrayFormatos,
      usuario: :string 
  end