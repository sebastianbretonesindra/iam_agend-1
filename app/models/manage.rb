class Manage < ActiveRecord::Base
  extend Admin::ManagesHelper
  belongs_to :user
  belongs_to :holder

  # searchable do
  #   integer :user_id  
  #   integer :holder_id  
  # end
end
