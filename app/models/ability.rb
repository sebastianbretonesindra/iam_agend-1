class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new

    if user.admin?
      can :manage, :all
    elsif user.lobby?
      can [:index, :new, :create, :show], Event
      can [:edit, :update], Event, status: "requested"
      cannot [:download], Event
      can [:create, :edit, :delete], Export
      if !user.role_view.blank?
        can [:show], Organization
        can :manage, OrganizationInterest
        can :manage, Agent
      else
        can [:show], Organization, id: user.organization_id
        can :manage, OrganizationInterest, organization_id: user.organization_id
        can :manage, Agent, organization_id: user.organization_id
      end
      cannot :destroy, Agent
      can [:clone,:export_outlook, :redirect, :callback], Event 
    elsif user.user?
      if Holder.managed_by(user.id).any? || !user.role_view.blank?
        can :manage, Event, id: Event.ability_titular_events(user)
        can :show, Event, id: Event.ability_events(user)
        can :create, Event
      else
        can :index, Event
      end
      can [:create, :clone, :export_outlook, :redirect, :callback], Event 
      can [:create, :edit, :delete], Export
      can [:show, :index], Organization
    end

    can [:revert], User
  end
end
