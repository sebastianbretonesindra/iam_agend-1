class RepresentedEntity < ActiveRecord::Base
  include ValidationHelper

  enum range_fund: [:range_1, :range_2, :range_3, :range_4]

  belongs_to :category
  belongs_to :address, dependent: :destroy
  belongs_to :organization, inverse_of: :represented_entities
  has_one :legal_representant,as: :entity, dependent: :destroy
  has_many :represented_range_subventions,class_name: "RangeSubvention" ,as: :item, dependent: :destroy

  # validates :identifier, :name, :from, :category_id, presence: true, if: :validate_organization
  # validate :document_identifier

  accepts_nested_attributes_for :address, allow_destroy: true
  accepts_nested_attributes_for :represented_range_subventions, allow_destroy: true
  accepts_nested_attributes_for :legal_representant, update_only: true, allow_destroy: true

  scope :by_organization, -> (organization_id) { where(organization_id: organization_id) }

  def fullname
    str = name
    str += " #{first_surname}"  if first_surname.present?
    str += " #{second_surname}" if second_surname.present?
    str
  end


  private

  def document_identifier
    if identifier_type.to_s =="DNI/NIF" && !validadorNIF_DNI_NIE(identifier)
      self.errors.add(:identifier, "El documento no tiene una validación correcta de DNI/NIF")    
    elsif identifier_type.to_s =="NIE" && !validadorNIF_DNI_NIE(identifier)
      self.errors.add(:identifier, "El documento no tiene una validación correcta de NIE") 
    elsif identifier_type.to_s =="Pasaporte" && !validatorPasaport(identifier)
      self.errors.add(:identifier, "El documento no tiene una validación correcta de Pasaporte")   
    end

    if identifier_type.blank?
      self.errors.add(:identifier_type, "Es necesario incluir el tipo de documento del representante legal")  
    end
  end
  
  def validate_organization
    !organization.blank? && organization.foreign_lobby_activity
  end
end
