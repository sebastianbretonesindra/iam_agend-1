class ManageEmail < ActiveRecord::Base
    validates :sender, presence: true
    
    validate :valid_sender
    validate :valid_cc
    validate :valid_cco
    validate :valid_excluded

    private 

    def valid_sender
        reg=/\A([\w+\-].?)+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i
        if !self.sender.blank? && self.sender.match(reg).to_s!=self.sender.to_s
            self.errors.add(self.sender,I18n.t("backend.manage_mails.sender.error"))
        end
    end

    def valid_cc
        validator_c(self.fields_cc, :fields_cc)
    end

    def valid_cco
        validator_c(self.fields_cco, :fields_cco)
    end

    def valid_excluded
        validator_c(self.excluded_emails, :fields_cc)
    end

    def validator_c(field, error)
        reg=/(\A([\w+\-].?)+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z)|(\A(([\w+\-].?)+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+;)+(([\w+\-].?)+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+)\z)/i
        if !field.blank? && reg.match(field).to_s!=field.to_s
            if error.to_s == "fields_cc"
                errors.add(field, I18n.t("backend.manage_mails.fields_cc.error"))
            else
                errors.add(field, I18n.t("backend.manage_mails.fields_cco.error"))
            end
            
        end
    end
end
